# Train and run the model

Download the training data from https://ai.stanford.edu/~amaas/data/sentiment/ and unpack in local folder called `aclImdb`.

In the Jupyter notebook `0 Train Model.ipynb` the training is explained. To simply train and save the model run 

```
python save_model.py
```

and to test it out check `load_model.py`

# Create environment

```
python -m venv env
source env/bin/activate
```

and then install the necessary libraries.

```
pip install scikit-learn==0.22
pip install pandas
```

# Run API

```
pip install fastapi uvicorn
python -m uvicorn app:app --reload
```

# Test API

We can test the API response time with `apache bench`.


First we test the general response of our API
```
ab -n 100 -c 20 http://127.0.0.1:8000/
```

and then we test the prediction endpoints

```
ab -n 100 -c 20 http://127.0.0.1:8000/predict?text=Hi+everyone%2C+I+am+pumped+for+API+development%21
```

# Docker

Build a docker image to wrap our API.

```
docker build -t datanatives-api ./
```

And then we can run the image in a container:

```
docker run -p 8000:8000 datanatives-api
```

# Kubernetes

Frist we need to upload the image to a registry so that we can access it from the cloud.

```
docker tag datanatives-api eu.gcr.io/datanatives-workshop/api
docker push eu.gcr.io/datanatives-workshop/api
```

When a cluster is ready we can deploy our app

```
kubectl create deployment datanatives-api --image=eu.gcr.io/datanatives-workshop/api
```

and then have to expose it to the internet.

```
kubectl expose deployment datanatives-api --port=8000 --target-port=8000 --type=LoadBalancer
ab -c 250 -n 10000 http://<ENTER_IP>:8000/predict_fast?text=Hi+everyone%2C+I+am+pumped+for+API+development%21
```

when the load is too much to handle for one pod we can add more easily:

```
kubectl scale --replicas=20 deployment datanatives-api
```